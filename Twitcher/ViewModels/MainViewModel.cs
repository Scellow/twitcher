﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Newtonsoft.Json;
using RestSharp;
using Twitcher.Models;

namespace Twitcher.ViewModels
{
    public class MainViewModel : BindableBase
    {
        public MainViewModel()
        {
            _client = new RestClient(Twitch.BASE_URL);

            PlayInVlc = new DelegateCommand<TStream>(PlayInVlcExecute);
            OpenChat = new DelegateCommand<TStream>(OpenChatExecute);

            SearchBackground = new SolidColorBrush(Color.FromRgb(149, 149, 149));
            SearchResults = new ObservableCollection<TGame>();
            GameResults = new ObservableCollection<TStream>();
        }

        #region Private Backing fields
        private readonly RestClient _client;
        private ObservableCollection<TStream> _gameResults;
        private string _search;
        private SolidColorBrush _searchBackground;
        private ObservableCollection<TGame> _searchResults;
        private TGame _selectedGame;
        #endregion

        #region Public Properties
        public string Search
        {
            get {return _search;}
            set
            {
                if(SetProperty(ref _search, value))
                {
                    SearchGames();
                }
            }
        }

        public ObservableCollection<TGame> SearchResults
        {
            get {return _searchResults;}
            set {SetProperty(ref _searchResults, value);}
        }

        public TGame SelectedGame
        {
            get {return _selectedGame;}
            set
            {
                if(SetProperty(ref _selectedGame, value))
                {
                    GetStreams();
                }
            }
        }

        public ObservableCollection<TStream> GameResults
        {
            get {return _gameResults;}
            set {SetProperty(ref _gameResults, value);}
        }

        public SolidColorBrush SearchBackground
        {
            get {return _searchBackground;}
            set {SetProperty(ref _searchBackground, value);}
        }

        public DelegateCommand<TStream> PlayInVlc {get;}
        public DelegateCommand<TStream> OpenChat {get;}
        #endregion

        #region Private Helper Methods
        private void SearchGames()
        {
            SearchResults.Clear();
            GameResults.Clear();

            var request = new RestRequest("search/games?q={query}&type=suggest", Method.GET);
            request.AddHeader("Accept", Twitch.ACCEPT_HEADER);
            request.AddUrlSegment("query", Search);

            _client.ExecuteAsync(request, SearchGamesExecute);
        }

        private void SearchGamesExecute(IRestResponse r)
        {
            if(r.ResponseStatus != ResponseStatus.Completed)
            {
                return;
            }

            var content = r.Content; // raw content as string
            if(string.IsNullOrEmpty(content))
            {
                return;
            }

            var response = JsonConvert.DeserializeObject<TSearchGameResponse>(content);

            if(response.IsEmpty)
            {
                Application.Current.Dispatcher.Invoke(() => SearchBackground.Color = Colors.Red);
            }
            else
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    SearchBackground.Color = Colors.Green;

                    foreach(var game in response.games)
                    {
                        SearchResults.Add(game);
                    }
                });
            }
        }

        private void GetStreams()
        {
            GameResults.Clear();

            if(SelectedGame == null)
            {
                return;
            }

            var request = new RestRequest("streams?game={game}", Method.GET);
            request.AddHeader("Accept", Twitch.ACCEPT_HEADER);
            request.AddUrlSegment("game", SelectedGame.name);

            _client.ExecuteAsync(request, GetStreamsExecute);
        }

        private void GetStreamsExecute(IRestResponse r)
        {
            if(r.ResponseStatus != ResponseStatus.Completed)
            {
                return;
            }

            var content = r.Content; // raw content as string
            if(string.IsNullOrEmpty(content))
            {
                return;
            }

            var response = JsonConvert.DeserializeObject<TStreamsResponse>(content);

            if(response.IsEmpty)
            {
                return;
            }

            Application.Current.Dispatcher.Invoke(() =>
            {
                foreach(var game in response.streams)
                {
                    GameResults.Add(game);
                }
            });
        }

        private void PlayInVlcExecute(TStream parm)
        {
            try
            {
                var p = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false,
                        FileName = @"C:\Program Files (x86)\Livestreamer\livestreamer.exe",
                        Arguments = parm.channel.url + " best",
                        CreateNoWindow = true
                    }
                };

                p.Start();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error loading VLC", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenChatExecute(TStream parm)
        {
            Process.Start(parm.channel._links.chat);
        }
        #endregion
    }
}