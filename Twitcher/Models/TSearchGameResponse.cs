﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Twitcher.Models
{
    public class TSearchGameResponse
    {
        public List<TGame> games = new List<TGame>();
        public TSearchGameResponse() {}

        public TSearchGameResponse(string content)
        {
            var tResponse = JsonConvert.DeserializeObject<TSearchGameResponse>(content);
            _links = tResponse._links;
            games = tResponse.games;
        }

        public LinksSearchResponse _links {get;set;}
        public bool IsEmpty => games.Count == 0;
    }
}