﻿namespace Twitcher.Models
{
    public class TGame
    {
        public string name {get;set;}
        public int popularity {get;set;}
        public int _id {get;set;}
        public int giantbomb_id {get;set;}
        public TBox box {get;set;}
        public TLogo logo {get;set;}
        public LinksGame _links {get;set;}
    }
}