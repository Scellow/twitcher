﻿namespace Twitcher.Models
{
    public class TStream
    {
        public string game {get;set;}
        public int viewers {get;set;}
        public double average_fps {get;set;}
        public int video_height {get;set;}
        public string created_at {get;set;}
        public long _id {get;set;}
        public TChannel channel {get;set;}
        public TPreview preview {get;set;}
        public LinksStream _links {get;set;}
    }
}