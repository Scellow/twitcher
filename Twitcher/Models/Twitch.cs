﻿namespace Twitcher.Models
{
    public class Twitch
    {
        public const string ACCEPT_HEADER = "application/vnd.twitchtv.v3+json";
        public const string BASE_URL = "https://api.twitch.tv/kraken";
        public const string SEARCH_CHANNELS = "/search/channels";
        public const string SEARCH_STREAMS = "/search/streams";
        public const string SEARCH_GAMES = "/search/games";
    }
}