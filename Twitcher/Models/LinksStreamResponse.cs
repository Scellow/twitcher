namespace Twitcher.Models
{
    public class LinksStreamResponse
    {
        public string summary {get;set;}
        public string followed {get;set;}
        public string next {get;set;}
        public string featured {get;set;}
        public string self {get;set;}
    }
}