﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Twitcher.Models
{
    public class TStreamsResponse
    {
        public List<TStream> streams = new List<TStream>();
        public TStreamsResponse() {}

        public TStreamsResponse(string content)
        {
            var tResponse = JsonConvert.DeserializeObject<TStreamsResponse>(content);
            _total = tResponse._total;
            streams = tResponse.streams;
        }

        public int _total {get;set;}
        public LinksStreamResponse _links {get;set;}
        public bool IsEmpty => streams.Count == 0;
    }
}